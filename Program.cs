﻿using System;
using PrototypeHW.Entities;
using PrototypeHW.Enums;

namespace PrototypeHW
{
    internal static class Program
    {
        private static void Main()
        {
            TestPersonCopy();
            TestUserCopy();
        }

        private static void TestPersonCopy()
        {
            var person1 = new Person
            {
                Name = "Alexey", LastName = "Moskovkin",
                PersonDetails = new PersonDetails()
                {
                    Birthday = DateTime.Parse("20.07.1986"),
                    City = "Moscow"
                },
                Gender = HumanGender.Male,
                State = HumanState.Alive
            };
            var person2 = person1.Copy();
            var person3 = person2.Clone();

            Console.WriteLine("Before change:");
            Console.WriteLine($" {nameof(person1)}. {person1}");
            Console.WriteLine($" {nameof(person2)}. {person2}");
            Console.WriteLine($" {nameof(person3)}. {person3}");

            person1.Name = "Alexander";
            person1.PersonDetails.Birthday = DateTime.Parse("19.06.1991");
            person1.PersonDetails.City = "Minsk";
            person1.State = HumanState.Unknown;

            person2.PersonDetails = new PersonDetails();
            person2.Name = "Elena";
            person2.LastName = "Ivanova";
            person2.Gender = HumanGender.Female;

            Console.WriteLine("After change:");
            Console.WriteLine($" {nameof(person1)}. {person1}");
            Console.WriteLine($" {nameof(person2)}. {person2}");
            Console.WriteLine($" {nameof(person3)}. {person3}");
        }

        private static void TestUserCopy()
        {
            var user1 = new User
            {
                Name = "Alexey", LastName = "Moskovkin", Login = "AMoskovkin", Password = "12345",
                PersonDetails = new PersonDetails()
                {
                    Birthday = DateTime.Parse("20.07.1986"),
                    City = "Moscow"
                },
                Gender = HumanGender.Male,
                State = HumanState.Alive,
                UserDetails = new UserDetails()
                {
                    Email = "test@email.com",
                    Rights = UserRights.Admin,
                    State = UserState.Authorized
                }
            };
            var user2 = (User) user1.Clone();
            var user3 = user2.Copy();

            Console.WriteLine("Before change:");
            Console.WriteLine($" {nameof(user1)}. {user1}");
            Console.WriteLine($" {nameof(user2)}. {user2}");
            Console.WriteLine($" {nameof(user3)}. {user3}");

            user1.Name = "Alexander";
            user1.Login = "AlexanderTheGreat";
            user1.Gender = HumanGender.Unknown;
            user1.PersonDetails.Birthday = DateTime.Parse("20.10.2000");
            user1.UserDetails.Rights = UserRights.Guest;
            user1.UserDetails.State = UserState.Unauthorized;

            user2.Name = "Elena";
            user2.Login = "ElEnA2020";
            user2.Gender = HumanGender.Female;
            user2.PersonDetails.Birthday = DateTime.Parse("12.11.1996");
            user2.PersonDetails.City = "Rostov";
            user2.UserDetails.Rights = UserRights.User;

            Console.WriteLine("After change:");
            Console.WriteLine($" {nameof(user1)}. {user1}");
            Console.WriteLine($" {nameof(user2)}. {user2}");
            Console.WriteLine($" {nameof(user3)}. {user3}");
        }
    }
}