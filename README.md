﻿# Домашнее задание по теме "Порождающие шаблоны"

## Цель

Создать иерархию из нескольких классов, в которых реализованы методы клонирования объектов по шаблону проектирования "
Прототип".

## Задания

- Придумать и создать 3-4 класса, которые как минимум дважды наследуются и написать краткое описание текстом.
- Создать свой интерфейс IMyCloneable для реализации шаблона "Прототип".
- Сделать возможность клонирования объекта для каждого из этих классов, используя вызовы родительских конструкторов.
- Составить тесты или написать программу для демонстрации функции клонирования.
- Добавить к каждому классу реализацию стандартного интерфейса ICloneable и реализовать его функционал через уже
  созданные методы.
- Написать вывод: какие преимущества и недостатки у каждого из интерфейсов: IMyCloneable и ICloneable.
- Напишите, сколько ушло времени на выполнение задания.

## Описание классов и перечислений

### Human

Класс содержит информацию об абстрактном человеке (Пол, Состояние)

### Person : Human

Класс содержит информацию о конкретном человеке (Имя, Фамилия)

### PersonDetails

Класс содержит более детальную информацию о конкретном человеке (Дата рождения, Город)

### User : Person

Класс содержит информацию о пользователе некоторой системы (Логин, Пароль)

### UserDetails

Класс содержит более детальную информацию о пользователе некоторой системы (Права доступа, Электронная почта, Состояние
авторизации)

### HumanGender

Пол человека

### HumanState

Состояние человека

### UserRights

Права доступа пользователя

### UserState

Состояние пользователя

## Вывод

### Стандартный интерфейс ICloneable:

- Недостатки ICloneable:
    - метод Clone() всегда возвращает тип object, а не конкретный тип, из-за чего происходит боксинг, что влияет на
      производительность
    - из-за типа object требуется явное приведение типа, происходит анбоксинг
    - при наследовании классов и реализации для каждого из них данного интерфейса не будет выдаваться предупреждений и
      ошибок, если интерфейс реализован только в базовом классе, что может привести к непредсказуемому поведению
      программы

- Преимущества ICloneable:

    - так как это стандартный интерфейс, то при использовании сторонних библиотек достаточно проверить "is ICloneable",
      чтобы быть уверенным, что данный класс реализует возможности копирования

### Своя реализация IMyCloneable:

- Недостатки:
    - нестандартный интерфейс, соответственно реализовать его и использовать все преимущества можно только в своих
      проектах
- Преимущества:
    - отсутствует боксинг/анбоксинг
    - не требуется явное приведение типа после операции копирования объекта
    - при наследовании классов и использовании generic варианта интерфейса в каждом из наследников есть гарантия, что
      программа не скомпилируется без реализации данного интерфейса в каждом из классов-наследников