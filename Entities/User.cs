﻿namespace PrototypeHW.Entities
{
    public class User : Person, IMyCloneable<User>
    {
        public string Login;
        public string Password;

        public UserDetails UserDetails = new();

        public override User Copy() => new()
        {
            Login = Login, Password = Password, Name = Name, LastName = LastName, UserDetails = UserDetails.Copy(),
            PersonDetails = PersonDetails.Copy(), Gender = Gender, State = State
        };

        public override object Clone() => Copy();

        public override string ToString() => $"{base.ToString()}, {Login}, {Password}, {UserDetails}";
    }
}