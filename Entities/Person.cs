﻿namespace PrototypeHW.Entities
{
    public class Person : Human , IMyCloneable<Person>
    {
        public string Name;
        public string LastName;

        public PersonDetails PersonDetails = new();

        public override Person Copy() => new()
            {Name = Name, LastName = LastName, PersonDetails = PersonDetails.Copy(), Gender = Gender, State = State};
        
        public override object Clone() => Copy();

        public override string ToString() => $"{Name}, {LastName}, {PersonDetails}, {base.ToString()}";
    }
}