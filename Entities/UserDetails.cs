﻿using System;
using PrototypeHW.Enums;

namespace PrototypeHW.Entities
{
    public class UserDetails : IMyCloneable<UserDetails>, ICloneable
    {
        public string Email;
        public UserRights Rights;
        public UserState State;

        public UserDetails Copy() => new() {Email = Email, Rights = Rights, State = State};

        public object Clone() => Copy();

        public override string ToString() => $"{Email}, {Rights}, {State}";
    }
}