﻿using System;

namespace PrototypeHW.Entities
{
    public class PersonDetails : IMyCloneable<PersonDetails>, ICloneable
    {
        public DateTime Birthday;
        public string City;

        public PersonDetails Copy() => new() {Birthday = Birthday, City = City};

        public object Clone() => Copy();

        public override string ToString() => $"{Birthday:d}, {City}";
    }
}