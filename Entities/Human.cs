﻿using System;
using PrototypeHW.Enums;

namespace PrototypeHW.Entities
{
    public class Human : IMyCloneable<Human>, ICloneable
    {
        public HumanGender Gender;
        public HumanState State;

        public virtual Human Copy() => new() {Gender = Gender, State = State};

        public virtual object Clone() => Copy();

        public override string ToString() => $"{Gender}, {State}";
    }
}