﻿namespace PrototypeHW
{
    public interface IMyCloneable<T>
    {
        T Copy();
    }
}