﻿namespace PrototypeHW.Enums
{
    public enum UserRights
    {
        Guest,
        User,
        Admin
    }
}