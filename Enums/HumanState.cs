﻿namespace PrototypeHW.Enums
{
    public enum HumanState
    {
        Unknown,
        Alive,
        Dead
    }
}