﻿namespace PrototypeHW.Enums
{
    public enum UserState
    {
        Unknown,
        Authorized,
        Unauthorized
    }
}