﻿namespace PrototypeHW.Enums
{
    public enum HumanGender
    {
        Unknown,
        Male,
        Female
    }
}